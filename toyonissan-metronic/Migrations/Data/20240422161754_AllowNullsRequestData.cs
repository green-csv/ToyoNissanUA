﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Starterkit.Migrations.Data
{
    /// <inheritdoc />
    public partial class AllowNullsRequestData : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ReferenceUserAutopartImages_ReferenceRequests_ReferenceRequestId",
                table: "ReferenceUserAutopartImages");

            migrationBuilder.AlterColumn<string>(
                name: "PhoneCountryCode",
                table: "ReferenceUserData",
                type: "nvarchar(32)",
                maxLength: 32,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(32)",
                oldMaxLength: 32);

            migrationBuilder.AlterColumn<string>(
                name: "Email",
                table: "ReferenceUserData",
                type: "nvarchar(128)",
                maxLength: 128,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(128)",
                oldMaxLength: 128);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RequestedDate",
                table: "ReferenceRequests",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2024, 4, 22, 16, 17, 54, 267, DateTimeKind.Utc).AddTicks(9639),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2024, 4, 22, 15, 56, 55, 783, DateTimeKind.Utc).AddTicks(4763));

            migrationBuilder.AddForeignKey(
                name: "FK_ReferenceUserAutopartImages_ReferenceRequests_ReferenceRequestId",
                table: "ReferenceUserAutopartImages",
                column: "ReferenceRequestId",
                principalTable: "ReferenceRequests",
                principalColumn: "Id");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ReferenceUserAutopartImages_ReferenceRequests_ReferenceRequestId",
                table: "ReferenceUserAutopartImages");

            migrationBuilder.AlterColumn<string>(
                name: "PhoneCountryCode",
                table: "ReferenceUserData",
                type: "nvarchar(32)",
                maxLength: 32,
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "nvarchar(32)",
                oldMaxLength: 32,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Email",
                table: "ReferenceUserData",
                type: "nvarchar(128)",
                maxLength: 128,
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "nvarchar(128)",
                oldMaxLength: 128,
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RequestedDate",
                table: "ReferenceRequests",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2024, 4, 22, 15, 56, 55, 783, DateTimeKind.Utc).AddTicks(4763),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2024, 4, 22, 16, 17, 54, 267, DateTimeKind.Utc).AddTicks(9639));

            migrationBuilder.AddForeignKey(
                name: "FK_ReferenceUserAutopartImages_ReferenceRequests_ReferenceRequestId",
                table: "ReferenceUserAutopartImages",
                column: "ReferenceRequestId",
                principalTable: "ReferenceRequests",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
