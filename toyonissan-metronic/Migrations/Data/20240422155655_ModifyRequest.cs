﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Starterkit.Migrations.Data
{
    /// <inheritdoc />
    public partial class ModifyRequest : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ReferenceRequests",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    OrderNum = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    RequestedDate = table.Column<DateTime>(type: "datetime2", nullable: false, defaultValue: new DateTime(2024, 4, 22, 15, 56, 55, 783, DateTimeKind.Utc).AddTicks(4763)),
                    Comments = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ReferenceRequests", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ReferenceUserAutopartImages",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ReferenceRequestId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    PictureReference = table.Column<byte[]>(type: "varbinary(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ReferenceUserAutopartImages", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ReferenceUserAutopartImages_ReferenceRequests_ReferenceRequestId",
                        column: x => x.ReferenceRequestId,
                        principalTable: "ReferenceRequests",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ReferenceUserData",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ReferenceRequestId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: false),
                    Email = table.Column<string>(type: "nvarchar(128)", maxLength: 128, nullable: false),
                    PhoneNumber = table.Column<string>(type: "nvarchar(32)", maxLength: 32, nullable: false),
                    PhoneCountryCode = table.Column<string>(type: "nvarchar(32)", maxLength: 32, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ReferenceUserData", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ReferenceUserData_ReferenceRequests_ReferenceRequestId",
                        column: x => x.ReferenceRequestId,
                        principalTable: "ReferenceRequests",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ReferenceUserAutopartImages_ReferenceRequestId",
                table: "ReferenceUserAutopartImages",
                column: "ReferenceRequestId");

            migrationBuilder.CreateIndex(
                name: "IX_ReferenceUserData_ReferenceRequestId",
                table: "ReferenceUserData",
                column: "ReferenceRequestId",
                unique: true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ReferenceUserAutopartImages");

            migrationBuilder.DropTable(
                name: "ReferenceUserData");

            migrationBuilder.DropTable(
                name: "ReferenceRequests");
        }
    }
}
