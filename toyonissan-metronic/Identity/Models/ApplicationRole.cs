﻿using Microsoft.AspNetCore.Identity;

namespace Starterkit.Identity.Models
{
    public class ApplicationRole : IdentityRole<Guid>
    {
    }
}
