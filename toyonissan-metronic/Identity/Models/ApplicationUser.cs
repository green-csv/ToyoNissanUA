﻿using Microsoft.AspNetCore.Identity;

namespace Starterkit.Identity.Models
{
    public class ApplicationUser:  IdentityUser<Guid>
    {
        public string Phone {  get; set; }
        public string PublicName { get; set; }
        public string Address { get; set; }

    }
}
