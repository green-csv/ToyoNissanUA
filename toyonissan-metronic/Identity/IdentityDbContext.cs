using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Starterkit.Identity.Models;


namespace Starterkit.Identity
{
   public class IdentityDbContext :
         IdentityDbContext<ApplicationUser, ApplicationRole, Guid,
         IdentityUserClaim<Guid>,
         IdentityUserRole<Guid>,
         IdentityUserLogin<Guid>,
         IdentityRoleClaim<Guid>,
         IdentityUserToken<Guid>>
   {

      public IdentityDbContext(DbContextOptions<IdentityDbContext> options)
            : base(options) { }
      protected override void OnModelCreating(ModelBuilder mb)
      {
         base.OnModelCreating(mb);

         mb.Entity<ApplicationUser>().ToTable("ToyoNissan_Users");
         mb.Entity<ApplicationRole>().ToTable("ToyoNissan_Roles");
         mb.Entity<IdentityUserClaim<Guid>>().ToTable("ToyoNissan_UserClaims");
         mb.Entity<IdentityUserRole<Guid>>().ToTable("ToyoNissan_UserRoles");
         mb.Entity<IdentityUserLogin<Guid>>().ToTable("ToyoNissan_UserLogins");
         mb.Entity<IdentityRoleClaim<Guid>>().ToTable("ToyoNissan_RoleClaims");
         mb.Entity<IdentityUserToken<Guid>>().ToTable("ToyoNissan_UserTokens");
      }

      private void Seed()
      {
         SaveChanges();

         // Make sure we have a SysAdmin role
         var role = Roles.FirstOrDefault(r => r.NormalizedName == "SYSADMIN");
         if (role == null)
         {
            role = new ApplicationRole
            {
               Id = Guid.NewGuid(),
               Name = "SysAdmin",
               NormalizedName = "SYSADMIN"
            };
            Roles.Add(role);
         }

         // Make sure our SysAdmin role has all of the available claims
         //foreach (var claim in Piranha.Security.Permission.All())
         //foreach (var permission in App.Permissions.GetPermissions())
         //{
         //    var roleClaim = RoleClaims.FirstOrDefault(c =>
         //        c.RoleId == role.Id && c.ClaimType == permission.Name && c.ClaimValue == permission.Name);
         //    if (roleClaim == null)
         //    {
         //        RoleClaims.Add(new IdentityRoleClaim<Guid>
         //        {
         //            RoleId = role.Id,
         //            ClaimType = permission.Name,
         //            ClaimValue = permission.Name
         //        });
         //    }
         //}

         SaveChanges();
      }
   }
}
