﻿
namespace Starterkit.Data.Models
{
    public class ReferenceUserData
    {
        public Guid Id { get; set; }
        public Guid ReferenceRequestId { get; set; }

        public string Name { get; set; }
        public string Email {  get; set; }
        public string PhoneNumber { get; set; }
        public string PhoneCountryCode { get; set; }

        public ReferenceRequest ReferenceRequest { get; set; }
    }
}
