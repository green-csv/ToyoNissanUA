﻿namespace Starterkit.Data.Models
{
    public class ReferenceRequest
    {
        public Guid Id { get; set; }

        public int OrderNum { get; set; }

        public DateTime RequestedDate { get; set; }

        public string? Comments { get; set; }

        public ReferenceUserData ReferenceUserData { get; set; }

        public List<ReferenceUserAutopartImage>? ReferenceUserAutopartImages { get; set; }
    }
}
