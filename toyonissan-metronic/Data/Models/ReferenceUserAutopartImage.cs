﻿namespace Starterkit.Data.Models
{
    public class ReferenceUserAutopartImage
    {
        public Guid Id { get; set; }
        public Guid ReferenceRequestId { get; set; }
        public byte[] PictureReference { get; set; }

        public virtual ReferenceRequest ReferenceRequest { get; set; }
    }
}
