﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Starterkit.Data.Models;
using System.Data;


namespace Starterkit.Data
{
    public class ApplicationDbContext : DbContext
    {
        public DbSet<ReferenceRequest> ReferenceRequests { get; set; }

        public DbSet<ReferenceUserAutopartImage> ReferenceUserAutopartImages { get; set; }

        public DbSet<ReferenceUserData> ReferenceUserData { get; set; }


        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options) { }


        protected override void OnModelCreating(ModelBuilder mb)
        {
            base.OnModelCreating(mb);

            mb.Entity<ReferenceRequest>(e =>
            {
                e.HasKey(p => p.Id);

                e.Property(p => p.OrderNum).UseIdentityColumn().ValueGeneratedOnAdd();
                e.Property(p => p.RequestedDate).IsRequired().HasDefaultValue(DateTime.UtcNow).ValueGeneratedOnAdd();
                e.Property(p => p.Comments).HasMaxLength(500).IsRequired(false);


                e.HasOne(p => p.ReferenceUserData)
                .WithOne(p => p.ReferenceRequest)
                .HasForeignKey<ReferenceUserData>(f => f.ReferenceRequestId);

                e.HasMany(p => p.ReferenceUserAutopartImages)
                .WithOne(p => p.ReferenceRequest)
                .HasForeignKey(f => f.ReferenceRequestId)
                .IsRequired(false);

            });

            mb.Entity<ReferenceUserData>(e =>
            {
                e.HasKey(p => p.Id);

                e.Property(p => p.Name).HasMaxLength(256);
                e.Property(p => p.Email).HasMaxLength(128).IsRequired(false);
                e.Property(p => p.PhoneNumber).HasMaxLength(32);
                e.Property(p => p.PhoneCountryCode).HasMaxLength(32).IsRequired(false);
            });

            mb.Entity<ReferenceUserAutopartImage>(e =>
            {
                e.HasKey(p => p.Id);
                e.Property(p => p.PictureReference);
            });
        }
    }
}
