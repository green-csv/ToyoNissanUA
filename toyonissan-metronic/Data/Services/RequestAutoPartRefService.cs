﻿using Microsoft.EntityFrameworkCore.ChangeTracking;
using MimeKit;
using RazorEngine;
using RazorEngine.Templating;
using Starterkit.Data.Models;
using Starterkit.Pages.Models;
using Starterkit.Shared.Services;

namespace Starterkit.Data.Services
{
    public class RequestAutoPartRefService
    {
        ILogger _logger;

        IEmailService _emailService;

        readonly ApplicationDbContext _context;

        public RequestAutoPartRefService(
            ApplicationDbContext context,
            ILogger<RequestAutoPartRefService> logger, IEmailService emailService)
        {
            _logger = logger;
            _emailService = emailService;

            _context = context;
        }


        public async Task SendAutopartReference(ModelRequestAutopartRef autopartRef)
        {


            EntityEntry<ReferenceRequest> entityReferenceRequest;
            try
            {
                var referenceUserData = new ReferenceUserData()
                {
                    Id = Guid.NewGuid(),
                    Name = autopartRef.UserName,
                    Email = autopartRef.UserEmail,
                    PhoneNumber = autopartRef.UserLocalPhoneNumber,
                };

                entityReferenceRequest =
                await _context.ReferenceRequests.AddAsync(new ReferenceRequest()
                {
                    Id = Guid.NewGuid(),
                    Comments = autopartRef.OtherComments,
                    ReferenceUserData = referenceUserData,
                });
            }
            catch (Exception ex) { _logger.LogError(ex.Message, ex); throw new Exception(ex.Message, ex); }

            var mailMessage = new MimeMessage();
            var bodyBuilder = new BodyBuilder();
            var imagesCid = new List<string>();

            try
            {
                mailMessage.From.Add(new MailboxAddress("TYUA Administrador", "solicitud_repuesto@toyonissan.com"));
                mailMessage.To.Add(new MailboxAddress("TYUA Ventas", "ventas@toyonissan.com"));
                mailMessage.Subject = "Solicitud Repuesto TYUA_" + entityReferenceRequest.Entity.OrderNum;

                if (autopartRef?.AutoPartReferenceImages is not null)
                {
                    foreach (var imageData in autopartRef.AutoPartReferenceImages)
                    {
                        //var urlMon = new UrlMon();
                        //var mime = urlMon.getMimeFromStream(stream);
                        using (var imgStream = new MemoryStream(imageData)) ;
                        var cid = Guid.NewGuid();
                        bodyBuilder.LinkedResources.Add("image" + cid, imageData);
                        imagesCid.Add("image" + cid);
                    }
                }
            }
            catch (Exception ex) { _logger.LogError(ex.Message, ex); throw; }

            var _modelView = new
            {
                OrderNum = entityReferenceRequest.Entity.OrderNum,
                Name = autopartRef.UserName,
                Phone = autopartRef.UserLocalPhoneNumber,
                Email = autopartRef.UserEmail,
                AutoPart = autopartRef.AutoPartName,
                Comments = autopartRef.OtherComments,
                ReferenceImages = imagesCid,
            };

            string htmlRazorRender;
            try
            {
                var template = File.ReadAllText("./Shared/_Templates/_MailRequestAutoPart.cshtml");
                htmlRazorRender =
                    Engine.Razor.RunCompile(template, "_MailRequestAutoPart.cshtml", null, _modelView);

            }
            catch (Exception ex) { _logger.LogError(ex.Message, ex); throw; }

            try
            {
                bodyBuilder.HtmlBody = htmlRazorRender;
                mailMessage.Body = bodyBuilder.ToMessageBody();

                await _context.SaveChangesAsync();
                await _emailService.SendEmailAsync(mailMessage);
                
            }
            catch (Exception ex) { _logger.LogError(ex.Message, ex); throw; }
        }
    }
}
