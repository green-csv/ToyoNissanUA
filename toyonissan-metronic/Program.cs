using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Starterkit;
using Starterkit._keenthemes;
using Starterkit._keenthemes.libs;
using Starterkit.Data;
using Starterkit.Data.Services;
using Starterkit.Identity;
using Starterkit.Identity.Models;
using Starterkit.Shared.Services;

var builder = WebApplication.CreateBuilder(args);

#if DEBUG
builder.Services.AddDatabaseDeveloperPageExceptionFilter();
#endif


builder.WebHost.UseSentry(options =>
{
    options.Dsn = "https://ce93bd74533949a5870e8f21f032bedb@app.glitchtip.com/6482";
    options.EnableTracing = true;
    options.Debug = true;
});


// Add services to the container.

var connectionStringData = builder.Configuration["ConnectionStrings:AppDataConnection"];
var connectionStringIdentity = builder.Configuration["ConnectionStrings:IdentityConnection"];

builder.Services
	.AddDbContext<ApplicationDbContext>(options =>
		options.UseSqlServer(connectionStringData)
	);

builder.Services
	.AddDbContext<IdentityDbContext>(options =>
	{
		options.UseSqlServer(
			connectionStringIdentity,
			options => options.MigrationsAssembly(typeof(Program).Assembly.GetName().Name)
		);
	});
builder.Services
    .AddDefaultIdentity<ApplicationUser>()
    .AddEntityFrameworkStores<IdentityDbContext>()
    .AddDefaultTokenProviders();



builder.Services.AddRazorPages();
builder.Services.AddServerSideBlazor();
//builder.Services.AddSingleton<WeatherForecastService>();
builder.Services.AddSingleton<IKTTheme, KTTheme>();
builder.Services.AddSingleton<IBootstrapBase, BootstrapBase>();
builder.Services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

builder.Services.AddScoped<IEmailService, SmtpEmailService>();

builder.Services.AddScoped<RequestAutoPartRefService>();


var app = builder.Build();

IConfiguration themeConfiguration = new ConfigurationBuilder()
                            .AddJsonFile("_keenthemes/config/themesettings.json")
                            .Build();

IConfiguration iconsConfiguration = new ConfigurationBuilder()
                            .AddJsonFile("_keenthemes/config/icons.json")
                            .Build();

KTThemeSettings.init(themeConfiguration);
KTIconsSettings.init(iconsConfiguration);

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();

app.UseStaticFiles();

app.UseRouting();

app.MapBlazorHub();
app.MapFallbackToPage("/_Host");

app.Run();
