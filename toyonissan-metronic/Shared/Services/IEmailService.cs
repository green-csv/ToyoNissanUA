﻿using MimeKit;

namespace Starterkit.Shared.Services
{
    public interface IEmailService
    {
        Task SendEmailAsync(MimeMessage mailMessage, List<Stream>? bodyStreams = null);
    }
}
