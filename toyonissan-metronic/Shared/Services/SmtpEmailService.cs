﻿using MimeKit;
using MimeKit.Utils;
using RazorEngine;
using RazorEngine.Templating;
using System.Net.Mail;
using System.Runtime.InteropServices;



namespace Starterkit.Shared.Services
{
    public class SmtpEmailService : IEmailService
    {
        private readonly ILogger<SmtpEmailService> _logger;
        
        public SmtpEmailService(ILogger<SmtpEmailService> logger)
        {
            _logger = logger;
            
        }

        public Task SendEmailAsync(MimeMessage mailMessage, List<Stream>? bodyStreams = null)
        {
            
            using (var client = new MailKit.Net.Smtp.SmtpClient())
            {
                //gmail
                client.Connect("smtp.gmail.com", 587, MailKit.Security.SecureSocketOptions.StartTls);
                client.Authenticate("user", "password");
                client.SendAsync(mailMessage);
                client.DisconnectAsync(true);
            }

            return Task.CompletedTask;
        }

    }

    public class UrlMon
    {
        [DllImport(@"urlmon.dll", CharSet = CharSet.Auto)]
        private extern static System.UInt32 FindMimeFromData(
        System.UInt32 pBC,
        [MarshalAs(UnmanagedType.LPStr)] System.String pwzUrl,
        [MarshalAs(UnmanagedType.LPArray)] byte[] pBuffer,
        System.UInt32 cbSize,
        [MarshalAs(UnmanagedType.LPStr)] System.String pwzMimeProposed,
        System.UInt32 dwMimeFlags,
        out System.UInt32 ppwzMimeOut,
        System.UInt32 dwReserverd);

        public UrlMon(){}


        public string getMimeFromStream(Stream fileStream)
        {
            byte[] buffer = new byte[256];
            try
            {
                using (Stream fs = fileStream)
                {
                    if (fs.Length >= 256)
                        fs.Read(buffer, 0, 256);
                    else
                        fs.Read(buffer, 0, (int)fs.Length);
                }
            }
            catch (Exception ex)
            {
                return "";
            }

            try
            {
                System.UInt32 mimetype;
                FindMimeFromData(0, null, buffer, 256, null, 0, out mimetype, 0);
                System.IntPtr mimeTypePtr = new IntPtr(mimetype);
                string mime = Marshal.PtrToStringUni(mimeTypePtr);
                Marshal.FreeCoTaskMem(mimeTypePtr);
                return mime;
            }
            catch (Exception ex)
            {                
                return "";
            }
        }
    }
}
