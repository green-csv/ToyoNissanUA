﻿using Starterkit.Pages.Request;
using System.ComponentModel.DataAnnotations;

namespace Starterkit.Pages.Models
{
    public class ModelRequestAutopartRef
    {

        [Required]
        public string? UserName { get; set; }

        
        
        public string? UserCountryCode { get; set; }


        [Required(ErrorMessage ="Obligatorio")]
        [RegularExpression(@"^\+?[1-9]\d{1,14}$", ErrorMessage = "No parece un numero de telefono")]
        public string? UserLocalPhoneNumber { get; set; }

        
        public string? UserEmail { get; set; }

        [Required]
        public string? AutoPartName { get; set; }

        
        public List<byte[]>? AutoPartReferenceImages { get; set; }

        
        public string? OtherComments { get; set; }
    }
}


