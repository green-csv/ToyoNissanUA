window.validateNumericInput = function (element) {
    element.addEventListener('input', function (event) {
        var value = event.target.value;
        var numericValue = parseFloat(value);
        if (isNaN(numericValue)) {
            event.target.value = '';
        }
    });
}